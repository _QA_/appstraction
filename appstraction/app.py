from .app_router import AppRouter
from .page_registry import PageRegistry


class AppMeta(type):
    def __new__(mcs, name, bases, dct):
        app_cls = super().__new__(mcs, name, bases, dct)
        app_cls.registry = PageRegistry()
        return app_cls

    def register_page(self, page_cls):
        self.registry.add(page_cls)
        return page_cls

    def register_pages(self, *pages_cls):
        for page_cls in pages_cls:
            self.register_page(page_cls)


class App(metaclass=AppMeta):
    def __init__(self, driver, base_url):
        self.driver = driver
        self.base_url = base_url
        self.router = AppRouter(app=self)

    @property
    def current_page(self):
        return self.router.match(self.driver.current_url)

    def get_page(self, page_name, query=None, fragment=None, navigate=True, **variables):
        if navigate:
            url = self.router.generate(page_name, query=query, fragment=fragment, **variables)
            self.driver.get(url)
            self.driver.wait_until_page_loaded()
        page_cls = self.registry.get(page_name)
        page = page_cls(app=self)
        return page

    def wait_until_current_page_is(self, page_name, timeout=None):
        self.driver.wait_until(
            function=lambda context: (self.current_page or {}).get('name') == page_name,
            timeout=timeout or self.driver.default_wait_timeout,
        )

    def wait_until_current_page_is_not(self, page_name, timeout=None):
        self.driver.wait_until_not(
            function=lambda context: (self.current_page or {}).get('name') == page_name,
            timeout=timeout or self.driver.default_wait_timeout,
        )
