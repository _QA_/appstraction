import pytest

from appstraction.router import Route


@pytest.mark.parametrize('template,variables,query,fragment,exp_path', [
    (r'', {}, None, None, ''),
    (r'/', {}, None, None, '/'),
    (r'/one', {}, None, None, '/one'),
    (r'/one/two', {}, None, None, '/one/two'),
    (r'/{one:[a-z]{3}}/two', {'one': 'ONE'}, None, None, '/ONE/two'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}', {'one': 'ONE', 'three': 3}, None, None, '/ONE/two/3'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', {'one': 'ONE', 'three': 3}, None, None, '/ONE/two/3/four/'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', {'one': 'ONE', 'three': 3}, None, 'anchor', '/ONE/two/3/four/#anchor'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', {'one': 'ONE', 'three': 3}, {'a': 'b'}, None, '/ONE/two/3/four/?a=b'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', {'one': 'ONE', 'three': 3}, {'a': 'b', 'b': 1}, None, '/ONE/two/3/four/?a=b&b=1'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', {'one': 'ONE', 'three': 3}, {'a': ['b', 'b'], 'b': 1}, None, '/ONE/two/3/four/?a=b&a=b&b=1'),
])
def test_route_generate(template, variables, query, fragment, exp_path):
    route = Route('test:name', template)
    path = route.generate(query=query, fragment=fragment, **variables)
    assert path == exp_path


@pytest.mark.parametrize('name, template, url, exp_name, exp_variables, exp_query, exp_fragment', [
    ('test:name', r'^/one/2/three/4$', 'http://example.com/one/2/three/4/', None, {}, {}, None),
    ('test:name', r'^/one/2/three/4$', 'http://example.com/one/2/three/4', 'test:name', {}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', '/one/2/three/4', 'test:name', {'two': '2', 'four': '4'}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', 'http://example.com/one/2/three/4', 'test:name', {'two': '2', 'four': '4'}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', 'http://example.com/one/2/three/4?a=a&b=b', 'test:name', {'two': '2', 'four': '4'}, {'a': 'a', 'b': 'b'}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', 'http://example.com/one/2/three/4?a=a&a=b', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b']}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', 'http://example.com/one/2/three/4?a=a&a=b&c=c', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b'], 'c': 'c'}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', 'http://example.com/one/2/three/4?a=a&a=b&c=c#anchor', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b'], 'c': 'c'}, 'anchor'),
])
def test_route_match(name, template, url, exp_name, exp_variables, exp_query, exp_fragment):
    route = Route(name, template)
    match = route.match(url)
    if exp_name is None:
        assert match is None
    else:
        assert match == {
            'name': exp_name,
            'variables': exp_variables,
            'query': exp_query,
            'fragment': exp_fragment,
        }
