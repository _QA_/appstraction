from urllib.parse import urljoin

from appstraction.router import Router


class AppRouter(Router):
    def __init__(self, app):
        super().__init__()
        self.app = app
        for page_name, page_cls in self.app.registry.items:
            self.add(page_name, page_cls.path)

    def generate(self, name, query=None, fragment=None, **variables):
        path = super().generate(name, query=query, fragment=fragment, **variables)
        return urljoin(self.app.base_url, path)
