Appstraction
============

## Dev Setup

```bash
virtualenv -p `which python3` .venv
source .venv/bin/activate
pip install -e '.[tests]'
```

### Run tests
```bash
pytest
```
