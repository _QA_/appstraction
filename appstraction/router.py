# TODO: move to external package
# TODO: add tests
# TODO: add matching/generation for scheme, subdomains and base path
# TODO: add some error handling with useful error messages
# TODO: add docs

import re
from string import Formatter
from urllib.parse import urlsplit, urlunsplit, urlencode, parse_qs


def _parse_query(query):
    query = parse_qs(qs=query, keep_blank_values=True)
    query = {k: (v[0] if len(v) == 1 else v) for k, v in query.items()}
    return query


class Router:
    def __init__(self):
        self.registry = {}

    def add(self, name, template):
        self.registry[name] = Route(name, template)

    def generate(self, name, query=None, fragment=None, **variables):
        return self.registry[name].generate(query=query, fragment=fragment, **variables)

    def match(self, url):
        for route in self.registry.values():
            match = route.match(url)
            if match:
                return match


class Route:
    def __init__(self, name, template):
        self.name = name

        self._variable_names, self._format_string, self._regexp_string = self.parse_template(template)
        self._regexp = re.compile(self._regexp_string)

    @staticmethod
    def parse_template(pattern):
        variable_names = set()
        format_string = r''
        regexp_string = r''
        for literal_text, field_name, format_spec, _conversion in Formatter().parse(pattern):
            format_string += literal_text
            regexp_string += literal_text
            if field_name:
                if not format_spec:
                    raise ValueError('Each capturing group must have proper regexp assigned.')
                format_string += '{' + field_name + '}'
                regexp_string += '(?P<' + field_name + '>' + format_spec + ')'
                variable_names.add(field_name)
        format_string = format_string.lstrip('^').rstrip('$')
        return variable_names, format_string, regexp_string

    def generate(self, query=None, fragment=None, **variables):
        path = self._format_string.format(**variables)
        path = urlunsplit(('', '', path, urlencode(query or '', doseq=True), fragment or ''))
        return path

    def match(self, url):
        _, _, path, query, fragment = urlsplit(url)
        match = self._regexp.match(path)
        if not match:
            return None
        return {
            'name': self.name,
            'variables': match.groupdict(),
            'query': _parse_query(query),
            'fragment': fragment or None
        }
