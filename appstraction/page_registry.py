from inspect import isclass

from .page import Page


class PageRegistry:
    def __init__(self):
        self._data = {}

    @property
    def names(self):
        return set(self._data.keys())

    @property
    def classes(self):
        return set(self._data.values())

    @property
    def items(self):
        return self._data.items()

    def _validate_page(self, page_cls):
        page_path = f'{Page.__module__}.{Page.__name__}'
        if not isclass(page_cls):
            raise ValueError(f'Page class must be class object got "{type(page_cls)}".')
        elif not issubclass(page_cls, Page):
            raise ValueError(f'{page_path} class must be subclass of "{Page.__module__}.{Page.__name__}".')
        elif page_cls.name is None:
            raise ValueError(f'{page_path}.name attribute must not be set to "None".')
        elif not isinstance(page_cls.path, str):
            raise ValueError(f'{page_path}.path attribute must be string.')
        elif page_cls in self.classes:
            raise ValueError(f'{page_path} has been already registered.')
        elif page_cls.name in self.names:
            raise ValueError(f'Page class with name "{page_cls.name}" has been already registered.')

    def add(self, page_cls):
        self._validate_page(page_cls)
        self._data[page_cls.name] = page_cls

    def get(self, page_name):
        return self._data[page_name]
