class Page:
    name = None
    path = None

    def __init__(self, app):
        self.app = app

    @property
    def driver(self):
        return self.app.driver
